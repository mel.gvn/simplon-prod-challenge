<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    //
    protected $fillable = ['title', 'start', 'end'];

    public function students()
    {
    	return $this->hasMany(Student::class);
    }
}
